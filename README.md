<!--
title: 'AWS Simple HTTP Endpoint example in NodeJS'
description: 'This template demonstrates how to make a simple HTTP API with Node.js running on AWS Lambda and API Gateway using the Serverless Framework.'
layout: Doc
framework: v3
platform: AWS
language: nodeJS
authorLink: 'https://github.com/serverless'
authorName: 'Serverless, inc.'
authorAvatar: 'https://avatars1.githubusercontent.com/u/13742415?s=200&v=4'
-->

# Serverless Framework Node API REST on AWS

You can find detailed information [here](https://docs.google.com/document/d/1UVhm75N96eOJZAKi2DIZk1bEf6kRgfSOlnM3BigbX_A/edit?usp=sharing)

### Requirements to run the project

- You need to install node js
- Run npm i to install dependencies
```bash
npm i
```

### Local development


You can invoke your function locally by using the following command:

```bash
serverless invoke local --function hasMutation  --data '{"dna":["CTGCGA", "CAGTTC" ...'
serverless invoke local --function stats
```

Which should result in response similar to the following:

```
{
  "statusCode": 200,
  "response": "Registro de ADN ha sido analizado exitosamente"
}
```

## Usage

### Deployment

```
$ serverless deploy
```

After deploying, you should see output similar to:

```bash
Deploying aws-node-http-api-project to stage dev (us-east-1)

✔ Service deployed to stack aws-node-http-api-project-dev (152s)

endpoint: GET - https://xxxxxxxxxx.execute-api.us-east-1.amazonaws.com/
functions:
  hello: aws-node-http-api-project-dev-hello (1.9 kB)
```

### Invocation

After successful deployment, you can call the created application via HTTP:

```bash
curl https://xxxxxxx.execute-api.us-east-1.amazonaws.com/
```

Which should result in response similar to the following (removed `input` content for brevity):

```json
{
  "message": "Go Serverless v2.0! Your function executed successfully!",
  "input": {
    ...
  }
}
```
