const SEQUENCE_PATTERN = 4;
const MIN_MUTATIONS = 2;
const PATTERN_A = new RegExp('A'.repeat(SEQUENCE_PATTERN),'gi');
const PATTERN_T = new RegExp('T'.repeat(SEQUENCE_PATTERN),'gi');
const PATTERN_C = new RegExp('C'.repeat(SEQUENCE_PATTERN),'gi');
const PATTERN_G = new RegExp('G'.repeat(SEQUENCE_PATTERN),'gi');
const PATTERN_VALIDATE = /[^ATCG]/i;


module.exports = {
  MIN_MUTATIONS,
  PATTERN_A,
  PATTERN_T,
  PATTERN_C,
  PATTERN_G,
  PATTERN_VALIDATE
};
