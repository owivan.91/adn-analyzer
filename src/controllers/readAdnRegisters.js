const AWS = require("aws-sdk");
const { getADNStats } = require("../actions");

const readADNStats = async () => {
  try {
    const { Count, ScannedCount } = await getADNStats();
    const countNoMutation = ScannedCount - Count;

    const response = {
      count_mutations: Count,
      count_no_mutation: countNoMutation,
      ratio: Count / countNoMutation
    };

    return {
      status: 200,
      body: response
    };
  } catch (error) {
    return {
      status: 403,
      body:{
        message: error
      }
    };
  }
};

module.exports = {
  readADNStats
};
