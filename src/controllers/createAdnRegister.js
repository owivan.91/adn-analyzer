const {
  MIN_MUTATIONS,
  PATTERN_A,
  PATTERN_T,
  PATTERN_C,
  PATTERN_G,
  PATTERN_VALIDATE
} = require('../constants/patternAdn');

const { saveAdnChain } = require('../actions');

const findMutationsByChain = (sequences) => {
  const mutations = sequences.filter(( item ) => {
    if(PATTERN_VALIDATE.test(item)) throw new Error('Cadena con caracteres invalidos');

     return item.match(PATTERN_C)
     || item.match(PATTERN_A)
     || item.match(PATTERN_T)
     || item.match(PATTERN_G);
  });
  return mutations.length;
};

const analyzeObliquePattern = (chain) => {
  const rowLength = chain.length;
  const columnLength = chain[0].length;
  const sequence = [];

  for (index = 0; index <= (rowLength + columnLength-2); index++) {
    let elementChain = '';

      for (let element = 0; element <= index; element++) {
      if (chain[index-element] && chain[index-element][element]){
          elementChain += chain[index-element][element];
      }
    }
     sequence.push(elementChain);
  }
  return sequence;
};

const analyzeHorizantalAndVerticalPattern = (chain) => {
  const longitudVertical= chain.length;
  let mutationsFound = 0;
   for (let index = 0; index < longitudVertical; index++) {
    	let chainByColumn = '';
      const chainByRow = chain[index];
     	const rowLength = chain[index].length;
     	for(let element = 0; element < rowLength; element++) {
       	chainByColumn += chain[element][index];
      }
     	mutationsFound += findMutationsByChain([chainByColumn, chainByRow]);
  	}
  	const obliqueSequences = analyzeObliquePattern(chain);
  	const obliqueMutationsFound = findMutationsByChain(obliqueSequences);

  return { mutationsFound, obliqueMutationsFound };
};


const hasMutation = (chain) => {
  const { mutationsFound, obliqueMutationsFound } = analyzeHorizantalAndVerticalPattern(chain);
  const hasMutations = mutationsFound + obliqueMutationsFound >= MIN_MUTATIONS;
  return hasMutations;
};

const analyzeADNChain = async (event) => {
  try {
    const { dna } = event;
    if(!dna) throw new Error('Envia una cadena Adn para analizar')
    const adnChainInformation = {
      dna,
      hasMutation: hasMutation(dna),
    };

    await saveAdnChain(adnChainInformation);

    return {
      status: 200,
      message: "Registro de ADN ha sido analizado exitosamente",
    };
  } catch (error) {
    return {
      status: 403,
      message: error
    };
  }
};

module.exports = {
  analyzeADNChain,
  findMutationsByChain,
  analyzeObliquePattern,
  analyzeHorizantalAndVerticalPattern,
  hasMutation
};
