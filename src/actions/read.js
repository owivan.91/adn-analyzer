const AWS = require("aws-sdk");

const getADNStats = async () => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const response = await dynamodb
    .scan({
      TableName: "AdnsTable",
      FilterExpression: "hasMutation = :value",
      ExpressionAttributeValues: {
        ":value": true
      }
    })
    .promise();

    return response;
  } catch (error) {
    return {
      statusCode: 403,
      body: {
        message: error
      }
    };
  }
};

module.exports = {
  getADNStats
};
