const { v4 } = require("uuid");
const AWS = require("aws-sdk");

const saveAdnChain = async (data) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const id = v4();

    const adnChainInformation = {
      id,
      ...data
    };

    await dynamodb
      .put({
        TableName: "AdnsTable",
        Item: adnChainInformation
      })
      .promise();

    return {
      message: "Registro de ADN ha sido almacenado exitosamente"
    };
  } catch (error) {
    throw new Error('Registro de ADN no se ha podido guardar correctamente');
  }
};

module.exports = {
  saveAdnChain
};
