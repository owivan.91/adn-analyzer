const {
  findMutationsByChain,
  analyzeObliquePattern,
  analyzeHorizantalAndVerticalPattern,
  hasMutation
} = require('../controllers/createAdnRegister');

const adnOblicuo = [
  "ATGCGA",
  "CTGTAC",
  "TTGACT",
  "TGACGG",
  "tGCCTA",
	"TCACTG"
];

const adn = [
  "ATGCGA",
  "CTGTAC",
  "TTGACT",
  "TGACGG",
  "tGCCTA",
	"TCACTG"
]

const expectResult = [
  "A",
  "CT",
  "TTG",
  "TTGC",
  "tGGTG",
  "TGAAAA",
  "CCCCC",
  "ACGT",
  "CTG",
  "TA",
  "G"
];

const adnNoMutations = [
  "ATGCGA",
  "CTGTAC",
  "TTGATT",
  "TGACGG",
  "tGCCTA",
	"ACACTG"
]

describe('Analizar sequencias en la cadena',() => {

  test('Validar adn sin mutacion', () => {
    expect(findMutationsByChain(['CAGTAC'])).toBe(0);
  });

  test('Buscar 1 mutacion en adn', () => {
    expect(findMutationsByChain(['CAAAAT', 'TAGCAA'])).toBe(1);
  });

  test('Buscar 2 mutaciones adn', () => {
    expect(findMutationsByChain(['CAAAAT', 'TAGGGG'])).toBe(2);
  });

  test('Error al encontrar caracteres invalidos', () => {
    expect( () => { findMutationsByChain(['CAAAAT', 'TAGGXG']) })
      .toThrow('Cadena con caracteres invalidos');
  });
});


describe('Buscar mutaciones oblicuas', () => {
  test('Obtener las combinaciones de sequencia de adn', () => {
    expect(analyzeObliquePattern(adnOblicuo)).toStrictEqual(expectResult);
  });
});

describe('Buscar mutaciones en ejes X & Y', () => {
  test('Buscar mutaciones', () => {
    expect(analyzeHorizantalAndVerticalPattern(adn)).toStrictEqual({ mutationsFound : 1, obliqueMutationsFound: 2 });
  });
});


describe('Buscar una mutacion en toda la cadena de adn ', () => {
  test('Buscar mutaciones', () => {
    expect(hasMutation(adn)).toBe(true);
  });

  test('Verificar adn sin mutaciones', () => {
    expect(hasMutation(adnNoMutations)).toBe(false);
  });
});
